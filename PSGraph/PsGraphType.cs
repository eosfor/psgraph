﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSGraph
{
	public enum PsGraphType
	{
		AdjacencyGraph, BidirectionalGraph, BidirectionalMatrixGraph, UndirectedGraph
	}
}
